<?php
/**
 * Created by PhpStorm.
 * User: Eric Mulder
 * Date: 06-04-2020
 * Time: 20:07
 */

namespace LabelVier\Models;


/**
 * Class Route
 * @package LabelVier\Models
 */
class Route {
	/**
	 * @var string
	 */
	public $path;
	/**
	 * @var string  Child class name of an EndpointController
	 */
	public $controller;
	/**
	 * @var string|array  Child class name(s) of an Guard
	 */
	public $guard;

	/**
	 * Route constructor.
	 *
	 * @param $path string          The full route path to match, can contain :id wilcards vars which will be converted to the $extraVars property in your endpoint controller.
	 * @param $controller string    Child class of an EndpointController, use like HelloWorldController::class
	 * @param $guard string         Child class of an Guard, use like HelloWorldController::class
	 */
	public function __construct($path, $controller, $guard = null) {
		$this->path = $path;
		$this->controller = $controller;
		$this->guard = $guard;
	}
}

<?php
/**
 * Created by PhpStorm.
 * User: Eric Mulder
 * Date: 09-04-2020
 * Time: 17:03
 */

namespace LabelVier\Controllers;


/**
 * Class EndpointController
 * @package LabelVier\Controllers
 */
abstract class EndpointController extends ResponseController {
	/**
	 * Extra args which are encapsuled in the route
	 *
	 * @var array
	 */
	public $extraArgs = [];

	/**
	 * Parse the incoming request, switch based on request method, the child classes implement those get / post / put / delete functions
	 */
	public function parseRequest() {
		switch ($_SERVER['REQUEST_METHOD']) {
			case 'GET': {
				$this->get($_GET, $this->extraArgs);
			} break;
			case 'POST': {
				$this->post($_POST, $this->extraArgs);
			} break;
			case 'PUT': {
				$_PUT = null;
				parse_str(file_get_contents('php://input'), $_PUT);
				$this->put($_PUT, $this->extraArgs);
			} break;
			case 'DELETE': {
				$_DELETE = null;
				parse_str(file_get_contents('php://input'), $_DELETE);
				$this->put($_DELETE, $this->extraArgs);
			} break;
		}
	}

	/**
	 * function for GET (default returns 404)
	 *
	 * @param $args
	 * @param $extraArgs
	 *
	 * @return void 404 not found header
	 */
	protected function get($args, $extraArgs) {
		//can be overridden by child class
		$this->notFound();
	}

	/**
	 * function for POST (default returns 404)
	 *
	 * @param $args
	 * @param $extraArgs
	 *
	 * @return void 404 not found header
	 */
	protected function post($args, $extraArgs) {
		//can be overridden by child class
		$request = RequestController::getInstance();
		$request->notFound();
	}

	/**
	 * function for PUT (default returns 404)
	 *
	 * @param $args
	 * @param $extraArgs
	 *
	 * @return void 404 not found header
	 */
	protected function put($args, $extraArgs) {
		//can be overridden by child class
		$request = RequestController::getInstance();
		$request->notFound();
	}

	/**
	 * function for DELETE (default returns 404)
	 *
	 * @param $args
	 * @param $extraArgs
	 *
	 * @return void 404 not found header
	 */
	protected function delete($args, $extraArgs) {
		//can be overridden by child class
		$request = RequestController::getInstance();
		$request->notFound();
	}

	/**
	 * Return function which echoes the supplied $args in `json_encode`
	 *
	 * @param mixed $args
	 */
	protected function returnJson($args) {
		header( 'HTTP/1.1 200 OK' );
		header('Content-Type: application/json');
		echo json_encode($args);
		exit;
	}
}

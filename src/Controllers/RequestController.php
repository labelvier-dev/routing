<?php
/**
 * Created by PhpStorm.
 * User: Eric Mulder
 * Date: 06-04-2020
 * Time: 19:40
 */

namespace LabelVier\Controllers;

use LabelVier\Guards\Guard;
use LabelVier\Models\Route;

/**
 * Class RequestController
 * @package LabelVier\Controllers
 */
class RequestController extends ResponseController {
	/**
	 * Singleton class
	 * @var null | RequestController
	 */
	private static $instance = null;
	/**
	 * Used to log 404 not found
	 *
	 * @var string
	 */
	private $requestMethod;
	/**
	 * The incoming route
	 *
	 * @var string
	 */
	private $route;

	/**
	 * Array of possible routes
	 *
	 * @var Route[]
	 */
	private $routes;

	/**
	 * Sets $this->routes variable
	 *
	 * @param $routes Route[]
	 */
	public function setRoutes($routes) {
		$this->routes = $routes;
	}

	/**
	 * Parse the incoming request, quickly return when we have missing variables
	 *
	 * @param string $route defaults to $_GET['route'], but can be changed to any (string) route.
	 */
	public function parseRequest($route = null) {
		$this->requestMethod = $_SERVER['REQUEST_METHOD'];
		$this->route         = $route ?: $_GET['route'];
		// check for api root and function, otherwise bail
		if ( ! $this->route ) {
			$this->notFound($this->route . ' not found for method ' . $this->requestMethod);
		}

		// get all possible routes and their actions
		return $this->parseRoutes();
	}

	/**
	 * Parse the route and start the correct controller
	 */
	private function parseRoutes() {

		$routeFound = false;
		foreach ( $this->routes as $route ) {
			// check for var in url path
			if (strpos($route->path, ':') !== false) {
				$routeParts = explode('/', $route->path);
				$routeVars = [];
				foreach($routeParts as &$part) {
					if($part[0] === ':') {
						$routeVars[] = substr($part, 1);
						// search for any characters in this spot in the url (except the slash)
						$part = '([^\/]+)';
					}
				}
				// create a regex search
				$route->path = '/'.implode('\/', $routeParts).'/i';
				preg_match($route->path, $this->route, $matches, PREG_OFFSET_CAPTURE, 0);
				// we found a match if we match the full route!
				if(count($matches) && $matches[0][0] === $this->route) {
					$routeFound = $this->parseRoute($route, $routeVars, $matches);
					break;
				}
			}
			elseif ( $route->path === $this->route ) {
				$routeFound = $this->parseRoute($route);
				break;
			}
		}

		// didn't find a route...
		if ( ! $routeFound ) {
			$this->notFound($this->route . ' not found for method ' . $this->requestMethod);
		} else {
			return $routeFound;
		}
	}

	/**
	 * Parse the found route
	 *
	 * @param Route $route
	 * @param bool $routeVars
	 * @param bool $extraArgs
	 *
	 * @return bool
	 */
	private function parseRoute($route, $routeVars = false, $extraArgs = false) {
		try {
			/**
			 * @var $routeController EndpointController
			 */
			if(!class_exists($route->controller)) {
				$this->notFound('controller ' . $route->controller .' not found for route ' . $this->route);
				return false;
			}
 			$routeController = new $route->controller;
			// check if we need to ar url path arguments
			if($extraArgs) {
				// add the extra variables, skip the full match
				array_shift($extraArgs);
				foreach ($extraArgs as $i => $arg) {
					$routeController->extraArgs[$routeVars[$i]] = $arg[0];
				}
			}
			// check for guard
			if($route->guard) {
				// allow an array of guards
				$guards = is_array($route->guard) ? $route->guard : [$route->guard];
				foreach ($guards as $guardClass) {
					/**
					 * @var $guard Guard
					 */
					$guard = new $guardClass;
					if(!$guard->canActivate($route, $extraArgs, $_SERVER['REQUEST_METHOD'])) {
						$this->notAllowed($this->route . ' forbidden for method ' . $this->requestMethod);
						return;
					}
				}
			}
			// parse the request
			$routeController->parseRequest();
			// everything succeeded
			return true;
		} catch ( \Exception $e ) {
			$this->notFound($this->route . ' not found for method ' . $this->requestMethod);
			return false;
		}
	}



	/**
	 * The object is created from within the class itself, only if the class has no instance.
	 *
	 * @return RequestController
	 */
	public static function getInstance()
	{
		if (self::$instance === null)
		{
			self::$instance = new self();
		}

		return self::$instance;
	}
}

<?php
/**
 * Created by PhpStorm.
 * User: Eric Mulder
 * Date: 26-04-2020
 * Time: 11:14
 */

namespace LabelVier\Controllers;


class ResponseController {
	/**
	 * Returns a 404 not found response
	 *
	 * @param $message
	 */
	public function notFound($message) {
		header( 'HTTP/1.1 404 Not Found' );
		echo $message;
		exit();
	}

	/**
	 * Returns a 403 forbidden response
	 *
	 * @param $message
	 */
	public function notAllowed($message) {
		header( 'HTTP/1.1 403 Forbidden' );
		echo $message;
		exit();
	}
}

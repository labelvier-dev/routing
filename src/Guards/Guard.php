<?php
/**
 * Created by PhpStorm.
 * User: Eric Mulder
 * Date: 10-04-2020
 * Time: 12:53
 */

namespace LabelVier\Guards;


/**
 * Class Guard
 * @package LabelVier\Guards
 */
abstract class Guard {
	/**
	 * Checks whether request can enter route
	 *
	 * @param Route $route
	 * @param string $extraArgs
	 * @param string $requestMethod
	 *
	 * @return boolean
	 */
	abstract public function canActivate($route, $extraArgs, $requestMethod);
}

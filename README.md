# Label Vier Routing #

Simple endpoint router inspired by Angular.

### What is this library for? ###

This library is used to create a really simple and quick routing request controller based on 3 principles:

1) Match an url (with optional wilcards) to an `EndpointController`   
2) Send the incoming variables to that `EndpointController`  
3) Guard the endpoint with a `Guard`  

### Installation with composer ###
```bash
$ composer require labelvier/routing
``` 

### Usage ###
1)  Create a .htaccess or ngix rewrite that rewrites your url to `index.php` with a `$_GET['route']`. So `/api/v1/hello-world/` becomes `index.php?route=/api/v1/hello-world/`.
2)  Require the `autoload.php` from your vendor folder.
3)  Init your `RequestController` and add some routes with custom`EndpointControllers` to it.
4)  Optionally add a `Guard` to your routes.
5)  That's it! You can now parse the request in your custom  `EndpointController`.

### Route child classes ###
A route consists of the endpoint to match, the `EndpointController` to activate on match and an optional `Guard` to guard the request.

You can add wilcards to your endpoint which are converted into `$extraArgs` in your `Guard` and `EndpointController`. 

Example:   
Using `/hello-world/:firstname/:lastname/`, `/hello-world/john/doe/` becomes `$extraArgs['firstname'] = 'john'` and `$extraArgs['lastname'] = 'joe'`. 

```php
new \LabelVier\Models\Route('/hello-world/:firstname/:lastname/', HelloWorldController::class, OptionalGuard::class);
```

### EndpointController child classes ###

A child class of the `EndpointController` can contain one of the following request methods: 
* `get($args, $extraArgs)`
* `post($args, $extraArgs)`
* `put($args, $extraArgs)`
* `delete($args, $extraArgs)`

If one of those functions is undefined, the response will automatically become a `404 not found error`.

For response, there is a basic `returnJson($args)`

```php
namespace Src\Controllers\EndpointController;

use LabelVier\Controllers\EndpointController;

/**
 * Simple example endpoint controller
 *
 * Class HelloWorldController
 * @package Src\Controllers\EndpointController
 */
class HelloWorldController extends EndpointController {

	/**
	 * get example
	 *
	 * @param $args
	 * @param $extraArgs
	 */
	public function get($args, $extraArgs) {
		if(count($extraArgs)) {
			$message = 'hello ' . $extraArgs['firstname'] . ' ' .  $extraArgs['lastname'];
		} else {
			$message = 'hello world';
		}

		if(isset($args['response'])) {
			$message .= ' - Your response: ' . $args['response'];
		}
    
                $this->returnJson(['response' => $message]);

	}
}
```

### Guard ###
A Guard class will simple return true or false in the `canActivate( $route, $extraArgs, $requestMethod )` to let the RequestController know if the request can pass. Here you can check for login state, tokens, etc.

```php
namespace Src\Guards;


use LabelVier\Guards\Guard;

class YouShallPassGuard extends Guard {

	/**
	 * Always pass :)
	 *
	 * @param Route $route
	 * @param string $extraArgs
	 * @param string $requestMethod
	 *
	 * @return bool
	 */
	public function canActivate( $route, $extraArgs, $requestMethod ) {
		return true;
	}
}
```

#### Sample setup. I.e. `index.php` (or other 'landing' php file) ####
An example file with all classes together. You can use any other variable as route, but than you need to set this in `$request->parseRequest($custom_route);`
```php
require( 'vendor/autoload.php' );


// routes
use LabelVier\Controllers\RequestController;
use LabelVier\Models\Route;
use Src\Controllers\EndpointController;
use Src\Guards;

// routes setup
$request = RequestController::getInstance();
$request->setRoutes([
	new Route( '/api/v1/login', EndpointController\LogInController::class ),
	new Route( '/api/v1/token-check', EndpointController\TokenCheckController::class, Guards\JWTGuard::class ),
	new Route( '/api/v1/hello-world', EndpointController\HelloWorldController::class),
	new Route( '/api/v1/hello-world/:firstname/:lastname', EndpointController\HelloWorldController::class, Guards\YouShallPassGuard::class ), // /api/v1/hello-world/label/vier
]);

// parse the request. The route var is optional and can be anything you want.
$request->parseRequest();
```

### Version history ###
#### 1.0.2 ####
* Better Route parsing and EndpointController / Guard setup
* Moved http responses to separate controller

#### 1.0.1 ####
* Parse request with optional route string.

#### 1.0.0 ####
* Initial import and readme.

### Contribution guidelines ###

* You can always create a pull request to help improve the library. 

### Who do I talk to? ###

* You can file issues or suggestions in the issue tracker on bitbucket.
* Or message the repo owner or admin (Label Vier).
